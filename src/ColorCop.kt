import java.util.*


internal abstract class Color : Cloneable {

    protected var colorName: String? = null

    internal abstract fun addColor()

    public override fun clone(): Any {
        var clone: Any? = null
        try {
            clone = super.clone()
        } catch (e: CloneNotSupportedException) {
            e.printStackTrace()
        }

        return clone!!
    }
}

internal class blueColor : Color() {
    init {
        this.colorName = "blue"
    }

    override fun addColor() {
        println("Blue color added")
    }

}

internal class blackColor : Color() {
    init {
        this.colorName = "black"
    }

    override fun addColor() {
        println("Black color added")
    }
}

internal object ColorStore {

    private val colorMap = HashMap<String, Color>()

    init {
        colorMap["blue"] = blueColor()
        colorMap["black"] = blackColor()
    }

    fun getColor(colorName: String): Color? {
        return colorMap[colorName]?.clone() as Color?
    }
}


// Driver class
internal object Prototype {
    @JvmStatic
    fun main(args: Array<String>) {
        ColorStore.getColor("blue")?.addColor()
        ColorStore.getColor("black")?.addColor()
        ColorStore.getColor("black")?.addColor()
        ColorStore.getColor("blue")?.addColor()
    }
}